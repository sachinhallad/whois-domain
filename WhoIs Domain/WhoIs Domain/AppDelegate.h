//
//  AppDelegate.h
//  WhoIs Domain
//
//  Created by hallad on 11/29/14.
//  Copyright (c) 2014 com.whois.domain. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

