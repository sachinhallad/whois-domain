//
//  main.m
//  WhoIs Domain
//
//  Created by hallad on 11/29/14.
//  Copyright (c) 2014 com.whois.domain. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
